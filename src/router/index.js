import Vue from 'vue'
import Router from 'vue-router'
import BookList from '@/components/BookList'
import BookView from '@/components/BookView'
import Admin from '@/components/admin/Admin'
import Login from '@/components/admin/Login'
import AddBook from '@/components/admin/AddBook'

import { getCookie } from '../../helpers/cookie';

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'BookList',
      component: BookList
    },
    {
      path: '/book/:id',
      name: 'BookView',
      component: BookView
    },

    {
        path: '/login',
        name: 'Login',
        component: Login,
    },

    {
        path: '/admin',
        name: 'Admin',
        component: Admin,
        meta: { requiresAuth: true },
        children: [
            {
                path: 'add',
                name: 'AddBook',
                component: AddBook,
            }
        ],
        beforeEnter: (to, from, next) => {
            var loggedIn = getCookie('loggedIn');
        //    if( to.matched.some(record => record.meta.requiresAuth) && !Auth.loggedIn ){
            if( to.matched.some(record => record.meta.requiresAuth) && !loggedIn ){
                next({
                    path: '/login',
                    query: {redirect: to.fullPath}
                })

            }else {
                next()
            }
        },
    }

  ]
})
