import Vue from 'vue';
import Vuex from 'vuex';

import axios from 'axios';
import lodash from 'lodash';

Vue.use(Vuex);

const state = {
    url: 'https://www.googleapis.com/books/v1/users/100785406093270558013/bookshelves/0/volumes?maxResults=1000&startIndex=0',
    books: [],
    totalBooks: 0,
    originalBooks: [],//value to hold the original items
    originalTotal: 0,//value to hold the original total number
    chosenCtg: 'reset',
};

const mutations = {
    SET_BOOK_LIST: (state, list) => {
        //console.log(list);
        state.books = _.sortBy(list, [function(row) { return row.volumeInfo.title; }]);
    },

    SET_BOOK_TOTAL: (state, total) => {
        state.totalBooks = total;
    },

    STORE_ORIGINAL_VALUES: (state, data) => {
        state.originalBooks = data.items;
        state.originalTotal = data.totalItems;
    },

    RESET: (state) => {
        state.books = state.originalBooks;
        state.totalBooks = state.originalTotal;
    },

    ADD_BOOK: (state, book) => {
        state.originalBooks.push(book);
        state.originalBooks = _.sortBy(state.originalBooks, [function(row) { return row.volumeInfo.title; }]);
        state.originalTotal++;
    },

    REMOVE_BOOK: (state, bookID) => {
        //console.log(bookID);
        let removed = _.remove(state.originalBooks, function(book) {
            return book.id == bookID;
        });
        //console.log(state.originalBooks);
        state.originalTotal = state.originalBooks.length;
    },

    SET_CHOSEN_CATEGORY: (state, category) => {
        state.chosenCtg = category;
    }
};

const actions = {
    fetchBooks: function({ commit }) {
        axios.get(state.url)
        .then( function(response) {
            var data = response.data,
                items = _.sortBy(data.items, [function(row) { return row.volumeInfo.title; }]);
            commit( 'SET_BOOK_LIST', data.items );
            commit( 'SET_BOOK_TOTAL', data.totalItems );
            commit( 'STORE_ORIGINAL_VALUES', {
                items: items,
                totalItems: data.totalItems
            });
        });
    },

    setBook: function({commit}, list) {
        commit( 'SET_BOOK_LIST', list );
    },

    setBookTotal: function({commit}, total) {
        commit( 'SET_BOOK_TOTAL', total );
    },

    reset: function({commit}) {
        commit('RESET');
    },

    addBook: function({commit}, book) {
        commit( 'ADD_BOOK', book );
        commit('RESET');
    },

    removeBook: function({commit}, bookID) {
        commit('REMOVE_BOOK', bookID);
        commit('RESET');
        console.log(state.books);
    },

    setChosenCategory: function({commit}, category) {
        commit ('SET_CHOSEN_CATEGORY', category);
    }
};

const getters = {
    categories : state => {
        let categories = [];
        _.forEach( state.originalBooks, function(item){
            let value = _.get(item, 'volumeInfo.categories[0]');
            if( value ) categories.push(value);
        });
        return _.uniq(categories);
    }
};


export const store = new Vuex.Store({
    state,
    mutations,
    actions,
    getters
});
